import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: { main: '#b22222' }, // Purple and green play nicely together.
        secondary: { main: '#f9d135' }, // This is just green.A700 as hex.
    },
    typography: { useNextVariants: true, fontSize: 12 },
    overrides: {
        MuiButtonBase: {
            root: {
                '&$disabled': {
                    color: '#f9d135',
                },
            },
            disabled: {},
        },
    },
});

export default theme;
