import { createStyles } from '@material-ui/core/styles';

const overrideStyles = () =>
    createStyles({
        note: {
            fontFamily: 'serif',
            fontSize: '1.5rem',
            color: 'red',
        },
    });

export default overrideStyles;
