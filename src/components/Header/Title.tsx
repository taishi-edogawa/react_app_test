import * as React from 'react';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import { Route, Switch } from 'react-router';
import Typography from '@material-ui/core/Typography';
import { Theme } from '@material-ui/core/styles/createMuiTheme';

const styles = (theme: Theme) =>
    createStyles({
        headTitle: {
            backgroundColor: '#f8f8f8',
            fontSize: 16,
            lineHeight: 1,
            padding: '10px',
            color: theme.palette.primary.main,
        },
    });

export interface Props extends WithStyles<typeof styles> {}

class HeaderTitle extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
    }
    render() {
        const { classes } = this.props;
        return (
            <>
                <Typography component='h1' variant='body1' className={classes.headTitle}>
                    <Switch>
                        <Route path='/reading'>scan</Route>
                        <Route path='/printing'>print</Route>
                    </Switch>
                </Typography>
            </>
        );
    }
}

export default withStyles(styles)(HeaderTitle);
