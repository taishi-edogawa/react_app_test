import * as React from 'react';

import Button from 'components/Header/Button';
import Title from 'components/Header/Title';

const header: React.SFC = () => (
    <header>
        <Button />
        <Title />
    </header>
);

export default header;
