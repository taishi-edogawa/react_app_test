import * as React from 'react';
import * as PropTypes from 'prop-types';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const styles = createStyles({
    buttonWrap: {
        textAlign: 'right'
    },
    headButton: {
        borderLeft: '1px solid #515151',
        color: '#fff',
        lineHeight: 1
    }
});

export interface Props extends WithStyles<typeof styles> {}

function headerButton(props: Props) {
    const { classes } = props;
    return (
        <div className={classes.buttonWrap}>
            <Button className={classes.headButton}>button</Button>
        </div>
    );
}

headerButton.propTypes = {
    classes: PropTypes.object.isRequired
} as any;

export default withStyles(styles)(headerButton);
