import * as React from 'react';

import BottomMenu from 'components/SideMenu/BottomMenu';
import TopMenu from 'components/SideMenu/TopMenu';
import './style.css';

// Dividers@material ui

const sideMenu: React.SFC = () => (
    <aside className="sideMenu">
        <TopMenu />
        <BottomMenu />
    </aside>
);

export default sideMenu;
