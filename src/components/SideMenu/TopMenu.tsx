import * as React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import { NavLink } from 'react-router-dom';
import * as H from 'history';

// react-routerのlocationの型定義、historyも同じH.historyでOK
interface Location {
    location: H.Location;
}

// import { ITopMenu, topMenuProvider } from 'AppContext';

const styles = createStyles({
    topMenu: {
        flexGrow: 1,
        padding: 0
    },
    topMenuLi: {
        background: '#f8f8f8',
        marginBottom: '0.5rem'
    }
});

export interface Props extends WithStyles<typeof styles> {}

class TopMenu extends React.Component<Props, Location> {
    constructor(props: Props) {
        super(props);
    }
    private isActiveReading = () => {
        return /reading/.test(location.pathname);
    };
    private isActivePrinting = () => {
        return /printing/.test(location.pathname);
    };
    render() {
        const { classes } = this.props;
        return (
            <List className={`topMenu ${classes.topMenu}`}>
                <ListItem className={classes.topMenuLi}>
                    <a href="javascript:void(0)">初期設定</a>
                </ListItem>
                <ListItem className={classes.topMenuLi}>
                    <a href="javascript:void(0)">ファイル名/形式</a>
                </ListItem>
                <ListItem className={classes.topMenuLi}>
                    <NavLink exact={true} to={'/reading/feed'} activeClassName="active" isActive={this.isActiveReading}>
                        読み取り条件
                    </NavLink>
                </ListItem>
                <ListItem className={classes.topMenuLi}>
                    <NavLink to={'/printing/basic'} activeClassName="active" isActive={this.isActivePrinting}>
                        印刷条件
                    </NavLink>
                </ListItem>
            </List>
        );
    }
}

export default withStyles(styles)(TopMenu);
