import * as React from 'react';
import Icon from '@material-ui/core/Icon';
import 'components/SideMenu/style.css';

const bottomMenu: React.SFC = () => (
    <ul className="bottomMenu">
        <li>
            <button type="button">
                <Icon className="icon clear">clear</Icon>削除
            </button>
        </li>
        <li>
            <a href="javascript:void(0)">管理モードトップへ戻る</a>
        </li>
        <li>
            <a href="javascript:void(0)">管理モード終了</a>
        </li>
    </ul>
);

export default bottomMenu;
