export interface SetVolume {
    weight: string;
    rep: string;
    set: string;
}

export interface InitialItems {
    inId?: number;
    name: string;
    color: string;
    type: string;
    setVolume?: SetVolume[];
}

export const initialItems: InitialItems[] = [
    {
        name: "apple",
        color: "red",
        type: "life",
    },
    {
        name: "tree",
        color: "blown",
        type: "life",
    },
    {
        name: "pen",
        color: "black",
        type: "inanimate",
    },
    {
        name: "mike",
        color: "silver",
        type: "inanimate",
    },
    {
        name: "dog",
        color: "blown",
        type: "life",
    },
    {
        name: "cat",
        color: "white",
        type: "life",
    },
    {
        name: "iphone",
        color: "red",
        type: "inanimate",
    },
    {
        name: "book",
        color: "white",
        type: "inanimate",
    },
    {
        name: "bench press",
        color: "chest",
        type: "compound",
    },
    {
        name: "dead lift",
        color: "back",
        type: "isolation",
    },
    {
        name: "squat",
        color: "leg",
        type: "inanimate",
    },
];
