import * as React from "react";

import { Route, Switch } from "react-router";

// import Tab from 'components/Main/Tab';

import Feed from "components/Main/Reading/Feed";
import Size from "components/Main/Reading/Size";
import Resolution from "components/Main/Reading/Resolution";
import Type from "components/Main/Reading/Type";
import Depth from "components/Main/Reading/Depth";
import ReadingEdit from "components/Main/Reading/ReadingEdit";

import Basic from "components/Main/Printing/Basic";
import PrintingEdit from "components/Main/Printing/PrintingEdit";
import ChangeTimes from "components/Main/Printing/ChangeTimes";
import Finish from "components/Main/Printing/Finish";

const main: React.SFC = () => {
    const [title, toggleTitle] = React.useState("");
    const titleChange = (data: string) => {
        toggleTitle(data);
    };

    React.useEffect(() => {
        toggleTitle(document.title);
    });
    const basic = () => <Basic.List title="Basic" titleChange={titleChange} />;
    const printingEdit = () => <PrintingEdit titleChange={titleChange} />;
    return (
        <div className="contents">
            {title}
            <Switch>
                <Route path="/reading/feed" component={Feed} />
                <Route path="/reading/size" component={Size} />
                <Route path="/reading/resolution" component={Resolution} />
                <Route path="/reading/type" component={Type} />
                <Route path="/reading/depth" component={Depth} />
                <Route path="/reading/edit" component={ReadingEdit} />

                <Route path="/printing/basic" render={basic} />
                <Route path="/printing/edit" render={printingEdit} />
                <Route path="/printing/changeTimes" component={ChangeTimes} />
                <Route path="/printing/finish" component={Finish} />
            </Switch>
        </div>
    );
};

export default main;
