export interface InitialFoods {
    name: string[];
    baseWeight: number;
    baseCal: number;
    protein: number;
    fat: number;
    carbo: number;
    category: string[];
}

export const initialFoods: InitialFoods[] = [
    {
        name: ['食品を選択して下さい'],
        baseWeight: 100,
        baseCal: 0,
        protein: 0,
        fat: 0,
        carbo: 0,
        category: ['']
    },
    {
        name: ['白米(炊き上がり)'],
        baseWeight: 160,
        baseCal: 269,
        protein: 4,
        fat: 0.48,
        carbo: 59.36,
        category: ['主食']
    },
    {
        name: ['玄米(炊き上がり)'],
        baseWeight: 160,
        baseCal: 264,
        protein: 4.48,
        fat: 1.6,
        carbo: 56.96,
        category: ['主食']
    },
    {
        name: ['米'],
        baseWeight: 150,
        baseCal: 534,
        protein: 9.15,
        fat: 1.35,
        carbo: 115.65,
        category: ['主食']
    },
    {
        name: ['じゃがいも'],
        baseWeight: 135,
        baseCal: 103,
        protein: 2.16,
        fat: 0.14,
        carbo: 23.76,
        category: ['主食', '野菜', '芋']
    },
    {
        name: ['さつまいも'],
        baseWeight: 200,
        baseCal: 264,
        protein: 2.4,
        fat: 0.4,
        carbo: 63,
        category: ['主食', '野菜', '芋']
    },
    {
        name: ['パスタ', 'スパゲッティ', 'マカロニ'],
        baseWeight: 250,
        baseCal: 373,
        protein: 13,
        fat: 2.25,
        carbo: 71,
        category: ['主食']
    },
    {
        name: ['食パン'],
        baseWeight: 60,
        baseCal: 158,
        protein: 5.58,
        fat: 2.64,
        carbo: 28.02,
        category: ['主食']
    },
    {
        name: ['薄力粉'],
        baseWeight: 110,
        baseCal: 405,
        protein: 8.8,
        fat: 1.87,
        carbo: 83.49,
        category: ['主食']
    },
    {
        name: ['強力粉'],
        baseWeight: 110,
        baseCal: 403,
        protein: 12.87,
        fat: 1.98,
        carbo: 87.76,
        category: ['主食']
    },
    {
        name: ['オートミール'],
        baseWeight: 80,
        baseCal: 304,
        protein: 10.96,
        fat: 4.56,
        carbo: 55.28,
        category: ['主食']
    },
    {
        name: ['鶏胸肉皮無し'],
        baseWeight: 100,
        baseCal: 108,
        protein: 22.3,
        fat: 1.5,
        carbo: 0,
        category: ['肉']
    },
    {
        name: ['鶏胸肉'],
        baseWeight: 100,
        baseCal: 191,
        protein: 19.5,
        fat: 11.6,
        carbo: 0,
        category: ['肉']
    },
    {
        name: ['ささみ'],
        baseWeight: 43,
        baseCal: 45,
        protein: 9.89,
        fat: 0.34,
        carbo: 0,
        category: ['肉']
    },
    {
        name: ['鶏肉'],
        baseWeight: 100,
        baseCal: 200,
        protein: 16.2,
        fat: 14,
        carbo: 0,
        category: ['肉']
    },
    {
        name: ['豚ロース'],
        baseWeight: 100,
        baseCal: 263,
        protein: 19.3,
        fat: 19.2,
        carbo: 0.2,
        category: ['肉']
    },
    {
        name: ['豚肩ロース'],
        baseWeight: 100,
        baseCal: 253,
        protein: 17.1,
        fat: 19.2,
        carbo: 0.1,
        category: ['肉']
    },
    {
        name: ['豚ばら肉'],
        baseWeight: 100,
        baseCal: 386,
        protein: 14.2,
        fat: 34.6,
        carbo: 0.1,
        category: ['肉']
    },
    {
        name: ['豚もも肉'],
        baseWeight: 100,
        baseCal: 183,
        protein: 20.5,
        fat: 10.2,
        carbo: 0.1,
        category: ['肉']
    },
    {
        name: ['豚ヒレ'],
        baseWeight: 100,
        baseCal: 115,
        protein: 22.8,
        fat: 1.9,
        carbo: 0.2,
        category: ['肉']
    },
    {
        name: ['牛肉かたロース'],
        baseWeight: 100,
        baseCal: 240,
        protein: 17.9,
        fat: 17.4,
        carbo: 0.1,
        category: ['肉']
    },
    {
        name: ['牛肉'],
        baseWeight: 100,
        baseCal: 371,
        protein: 14.4,
        fat: 32.9,
        carbo: 0.2,
        category: ['肉']
    },
    {
        name: ['醤油'],
        baseWeight: 18,
        baseCal: 13,
        protein: 1.39,
        fat: 0,
        carbo: 1.82,
        category: ['調味料']
    },
    {
        name: ['みりん'],
        baseWeight: 117,
        baseCal: 282,
        protein: 0.35,
        fat: 0,
        carbo: 50.54,
        category: ['調味料']
    },
    {
        name: ['日本酒', '料理酒'],
        baseWeight: 180,
        baseCal: 196,
        protein: 0.72,
        fat: 0,
        carbo: 8.82,
        category: ['調味料', 'アルコール']
    },
    {
        name: ['赤ワイン'],
        baseWeight: 80,
        baseCal: 58,
        protein: 0.16,
        fat: 0,
        carbo: 1.2,
        category: ['調味料', 'アルコール']
    },
    {
        name: ['白ワイン'],
        baseWeight: 80,
        baseCal: 58,
        protein: 0.08,
        fat: 0,
        carbo: 1.6,
        category: ['調味料', 'アルコール']
    },
    {
        name: ['ウィスキー'],
        baseWeight: 29,
        baseCal: 69,
        protein: 0,
        fat: 0,
        carbo: 0,
        category: ['調味料']
    },
    {
        name: ['塩'],
        baseWeight: 18,
        baseCal: 0,
        protein: 0,
        fat: 0,
        carbo: 0,
        category: ['調味料']
    },
    {
        name: ['砂糖'],
        baseWeight: 9,
        baseCal: 35,
        protein: 0,
        fat: 0,
        carbo: 8.93,
        category: ['調味料']
    },
    {
        name: ['キャベツ'],
        baseWeight: 1020,
        baseCal: 235,
        protein: 13.26,
        fat: 2.04,
        carbo: 53.04,
        category: ['野菜']
    },
    {
        name: ['白菜'],
        baseWeight: 940,
        baseCal: 132,
        protein: 7.52,
        fat: 0.94,
        carbo: 30.08,
        category: ['野菜']
    },
    {
        name: ['レタス'],
        baseWeight: 490,
        baseCal: 59,
        protein: 2.94,
        fat: 0.49,
        carbo: 13.72,
        category: ['野菜']
    },
    {
        name: ['たまねぎ'],
        baseWeight: 177,
        baseCal: 65,
        protein: 1.77,
        fat: 0.18,
        carbo: 15.58,
        category: ['野菜']
    },
    {
        name: ['にんじん'],
        baseWeight: 146,
        baseCal: 54,
        protein: 0.88,
        fat: 0.15,
        carbo: 13.14,
        category: ['野菜']
    },
    {
        name: ['ピーマン'],
        baseWeight: 26,
        baseCal: 6,
        protein: 0.23,
        fat: 0.05,
        carbo: 1.33,
        category: ['野菜']
    },
    {
        name: ['もやし'],
        baseWeight: 243,
        baseCal: 34,
        protein: 4.13,
        fat: 0.24,
        carbo: 6.32,
        category: ['野菜']
    },
    {
        name: ['オクラ'],
        baseWeight: 85,
        baseCal: 26,
        protein: 1.79,
        fat: 0.17,
        carbo: 5.61,
        category: ['野菜']
    }
];
