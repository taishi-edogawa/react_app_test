import * as React from 'react';

import { Route, Switch } from 'react-router';

import ReadingTab from 'components/Main/Tab/ReadingTab';
import PrintingTab from 'components/Main/Tab/PrintingTab';
import './style.css';

const tab: React.SFC = () => {
    return (
        <div className="tabWrap">
            <Switch>
                <Route path="/reading/*" component={ReadingTab} />
                <Route path="/printing/*" component={PrintingTab} />
            </Switch>
        </div>
    );
};

export default tab;
