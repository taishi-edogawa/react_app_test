import * as React from 'react';

import { NavLink } from 'react-router-dom';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import tabData from 'components/Main/Tab/ReadingTab/tabData';

const readingTab: React.SFC = () => {
    const { reading } = tabData;
    return (
        <List className="globalTab">
            {reading.map((key, i) => (
                <ListItem key={i}>
                    {key.enName === 'feed' ? (
                        <NavLink exact={true} to={`/reading/${key.enName}`} id={key.enName} activeClassName="active">
                            {key.jaName}
                        </NavLink>
                    ) : (
                        <NavLink to={`/reading/${key.enName}`} id={key.enName} activeClassName="active">
                            {key.jaName}
                        </NavLink>
                    )}
                </ListItem>
            ))}
        </List>
    );
};

export default readingTab;
