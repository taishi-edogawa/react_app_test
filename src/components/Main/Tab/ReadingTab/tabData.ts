interface NamePair {
    enName: string;
    jaName: string;
}

interface Tab {
    [globalMenu: string]: NamePair[];
}

const tabData: Tab = {
    reading: [
        {
            enName: 'feed',
            jaName: '原稿送り',
        },
        {
            enName: 'size',
            jaName: 'サイズ',
        },
        {
            enName: 'resolution',
            jaName: '解像度',
        },
        {
            enName: 'type',
            jaName: '原稿種類',
        },
        {
            enName: 'depth',
            jaName: '濃度',
        },
        {
            enName: 'edit',
            jaName: '編集',
        },
    ],
};

export default tabData;
