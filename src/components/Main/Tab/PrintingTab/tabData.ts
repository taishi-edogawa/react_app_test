interface NamePair {
    enName: string;
    jaName: string;
}

interface Tab {
    [globalMenu: string]: NamePair[];
}

const tabData: Tab = {
    printing: [
        {
            enName: 'basic',
            jaName: '基本',
        },
        {
            enName: 'edit',
            jaName: '編集',
        },
        {
            enName: 'changeTimes',
            jaName: '変倍',
        },
        {
            enName: 'finish',
            jaName: '仕上げ',
        },
    ],
};

export default tabData;
