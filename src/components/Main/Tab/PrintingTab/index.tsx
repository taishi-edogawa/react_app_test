import * as React from 'react';

import { NavLink } from 'react-router-dom';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import tabData from 'components/Main/Tab/PrintingTab/tabData';

const printingTab: React.SFC = () => {
    const { printing } = tabData;

    return (
        <List className="globalTab">
            {printing.map((key, i) => (
                <ListItem key={i}>
                    {key.enName === 'basic' ? (
                        <NavLink exact={true} to={`/printing/${key.enName}`} id={key.enName} activeClassName="active">
                            {key.jaName}
                        </NavLink>
                    ) : (
                        <NavLink to={`/printing/${key.enName}`} id={key.enName} activeClassName="active">
                            {key.jaName}
                        </NavLink>
                    )}
                </ListItem>
            ))}
        </List>
    );
};

export default printingTab;
