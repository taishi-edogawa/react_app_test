import * as React from 'react';

import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

interface StapleInterface {
    staple: string;
}

class Type extends React.Component<{}, StapleInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            staple: 'しない',
        };
    }

    handleChange = (event: React.FormEvent<HTMLInputElement>) => {
        this.setState({ staple: event.currentTarget.value });
    };

    private change = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.handleChange(event);
    };

    render() {
        return (
            <>
                <FormControl>
                    <RadioGroup name='type' value={this.state.staple} onChange={this.change} className='radioGroup'>
                        <FormControlLabel value='しない' control={<Radio />} label='しない' />
                        <FormControlLabel value='上2ヵ所' control={<Radio />} label='上2ヵ所' />
                        <FormControlLabel value='中央2ヵ所' control={<Radio />} label='中央2ヵ所' />
                        <FormControlLabel value='左2ヵ所' control={<Radio />} label='左2ヵ所' />
                        <FormControlLabel value='左上' control={<Radio />} label='左上' />
                        <FormControlLabel value='左上斜め' control={<Radio />} label='左上斜め' />
                        <FormControlLabel value='右2ヵ所' control={<Radio />} label='右2ヵ所' />
                        <FormControlLabel value='右上' control={<Radio />} label='右上' />
                        <FormControlLabel value='右上斜め' control={<Radio />} label='右上斜め' />
                    </RadioGroup>
                </FormControl>
            </>
        );
    }
}

export default Type;
