import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

interface PunchInterface {
    punch: string;
}

class Punch extends React.Component<{}, PunchInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            punch: 'しない'
        };
    }

    handleDirction = (event: React.MouseEvent, punch: string) => {
        this.setState({ punch });
    };

    render() {
        const punch = this.state.punch;

        return (
            <>
                <ToggleButtonGroup value={punch} exclusive={true} onChange={this.handleDirction}>
                    <ToggleButton value="しない" disabled={punch === 'しない'}>
                        しない
                    </ToggleButton>
                    <ToggleButton value="上2ヵ所" disabled={punch === '上2ヵ所'}>
                        上2ヵ所
                    </ToggleButton>
                    <ToggleButton value="左2ヵ所" disabled={punch === '左2ヵ所'}>
                        左2ヵ所
                    </ToggleButton>
                    <ToggleButton value="右2ヵ所" disabled={punch === '右2ヵ所'}>
                        右2ヵ所
                    </ToggleButton>
                </ToggleButtonGroup>
            </>
        );
    }
}

export default Punch;
