import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

interface SortInterface {
    format: string;
}

class Sort extends React.Component<{}, SortInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            format: 'スタック'
        };
    }

    handleDirction = (event: React.MouseEvent, format: string) => {
        this.setState({ format });
    };

    render() {
        const format = this.state.format;

        return (
            <>
                <ToggleButtonGroup value={format} exclusive={true} onChange={this.handleDirction}>
                    <ToggleButton value="スタック" disabled={format === 'スタック'}>
                        スタック
                    </ToggleButton>
                    <ToggleButton value="ソート" disabled={format === 'ソート'}>
                        ソート
                    </ToggleButton>
                    <ToggleButton value="回転ソート" disabled={format === '回転ソート'}>
                        回転ソート
                    </ToggleButton>
                    <ToggleButton value="シフトソート" disabled={format === 'シフトソート'}>
                        シフトソート
                    </ToggleButton>
                </ToggleButtonGroup>
            </>
        );
    }
}

export default Sort;
