import * as React from 'react';

import Punch from './Punch';
import Sort from './Sort';
import Staple from './Staple';

const finish: React.SFC = () => (
    <div className="finish">
        <div className="box">
            <p>ソート方式</p>
            <Sort />
        </div>
        <div className="box alignTop">
            <p>ステープル</p>
            <Staple />
        </div>
        <div className="box">
            <p>パンチ</p>
            <Punch />
        </div>
    </div>
);

export default finish;
