import * as React from 'react';

import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

interface FormatInterface {
    pdfIsChecked?: boolean;
    excelIsChecked?: boolean;
    wordIsChecked?: boolean;
    powerpointIsChecked?: boolean;
    agentConfirm?: boolean;
}

interface HTMLElementEvent<T extends HTMLElement> extends React.ChangeEvent {
    target: T;
}

class Format extends React.Component<{}, FormatInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            pdfIsChecked: true,
            excelIsChecked: false,
            wordIsChecked: false,
            powerpointIsChecked: false,
            agentConfirm: false,
        };
    }

    handleChange = (name: string) => (event: HTMLElementEvent<HTMLInputElement>) => {
        this.setState({ [name]: event.target.checked });
        console.log(this.state);
    };

    render() {
        return (
            <FormGroup row={true}>
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={this.state.pdfIsChecked}
                            onChange={this.handleChange('pdfIsChecked')}
                            value='pdfIsChecked'
                        />
                    }
                    label='PDF'
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={this.state.excelIsChecked}
                            onChange={this.handleChange('excelIsChecked')}
                            value='excelIsChecked'
                        />
                    }
                    label='Excel'
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={this.state.wordIsChecked}
                            onChange={this.handleChange('wordIsChecked')}
                            value='wordIsChecked'
                        />
                    }
                    label='Word'
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={this.state.powerpointIsChecked}
                            onChange={this.handleChange('powerpointIsChecked')}
                            value='powerpointIsChecked'
                        />
                    }
                    label='PowerPoint'
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={this.state.agentConfirm}
                            onChange={this.handleChange('agentConfirm')}
                            value='agentConfirm'
                        />
                    }
                    label='agentConfirm'
                />
            </FormGroup>
        );
    }
}

export default Format;
