import * as React from 'react';

import Button from '@material-ui/core/Button';
import ExposurePlus1 from '@material-ui/icons/ExposurePlus1';
import ExposureNeg1 from '@material-ui/icons/ExposureNeg1';
import Fab from '@material-ui/core/Fab';
import { createStyles, withStyles, WithStyles, Theme } from '@material-ui/core/styles';

interface NumberInterface {
    count: number;
}

const styles = () =>
    createStyles({
        count: {
            fontSize: '1.5rem',
            fontWeight: 'bold',
            textAlign: 'center',
            width: 48,
        },
    });

export interface Props extends WithStyles<typeof styles> {
    theme: Theme;
}

const number = withStyles(styles)(
    class extends React.Component<Props, NumberInterface> {
        state = {
            count: 0,
        };

        increment = () => {
            this.setState({ count: this.state.count + 1 });
        };

        decrement = () => {
            if (this.state.count > 0) {
                this.setState({ count: this.state.count - 1 });
            }
        };

        render() {
            const { classes } = this.props;
            return (
                <>
                    <Fab size='medium' area-label='Add'>
                        <ExposureNeg1 onClick={this.decrement} />
                    </Fab>
                    <div className={classes.count}>{this.state.count}</div>
                    <Fab size='medium' area-label='Add'>
                        <ExposurePlus1 onClick={this.increment} />
                    </Fab>
                    <Button>保存</Button>
                </>
            );
        }
    },
);

export default withStyles(styles, { withTheme: true })(number);
