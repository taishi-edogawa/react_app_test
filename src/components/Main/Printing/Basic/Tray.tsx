import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

interface TrayInterface {
    tray: string;
}

class Tray extends React.Component<{}, TrayInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            tray: '自動トレイ選択',
        };
    }

    handleDirction = (event: React.MouseEvent, tray: string) => {
        this.setState({ tray });
    };

    render() {
        const tray = this.state.tray;

        return (
            <div>
                <ToggleButtonGroup value={tray} exclusive={true} onChange={this.handleDirction}>
                    <ToggleButton value='自動トレイ選択' disabled={tray === '自動トレイ選択'}>
                        自動トレイ選択
                    </ToggleButton>
                    <ToggleButton value='手差しトレイ' disabled={tray === '手差しトレイ'}>
                        手差しトレイ
                    </ToggleButton>
                </ToggleButtonGroup>
            </div>
        );
    }
}

export default Tray;
