import * as React from "react";
import useTitle from "components/Main/useTitle";

import Color from "./Color";
import Format from "./Format";
import Number from "./Number";
import Tray from "./Tray";

interface Props {
    title: string;
    titleChange: (data: string) => void;
}

const basic: React.SFC<Props> = props => {
    const title = props.title;
    useTitle("Basic");
    React.useEffect(() => {
        props.titleChange(document.title);
    });

    return (
        <div className="basic">
            <h1>{title}</h1>
            <div className="box">
                <p>印刷対象形式</p>
                <Format />
            </div>
            <div className="box">
                <p>カラー／白黒</p>
                <Color />
            </div>
            <div className="box">
                <p>部数</p>
                <Number />
            </div>
            <div className="box">
                <p>給紙トレイ</p>
                <Tray />
            </div>
            <p className="annotation">
                選択した項目に応じて、同時選択不可の項目がグレーアウトします。
                <br />
                同時選択不可項目の詳細につきましては、操作マニュアルをご参照ください。
            </p>
        </div>
    );
};

export default basic;
