import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

interface ColorInterface {
    color: string;
}

class Color extends React.Component<{}, ColorInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            color: 'カラー'
        };
    }

    handleDirction = (event: React.MouseEvent, color: string) => {
        this.setState({ color });
    };

    render() {
        const color = this.state.color;

        return (
            <>
                <ToggleButtonGroup className="flex" value={color} exclusive={true} onChange={this.handleDirction}>
                    <ToggleButton value="カラー" disabled={color === 'カラー'}>
                        カラー向き
                    </ToggleButton>
                    <ToggleButton value="白黒" disabled={color === '白黒'}>
                        白黒
                    </ToggleButton>
                    <ToggleButton value="2色(黒・シアン)" disabled={color === '2色(黒・シアン)'}>
                        2色
                        <br />
                        (黒・シアン)
                    </ToggleButton>
                    <ToggleButton value="2色(黒・マゼンタ)" disabled={color === '2色(黒・マゼンタ)'}>
                        2色
                        <br />
                        (黒・マゼンタ)
                    </ToggleButton>
                    <ToggleButton value="2色(黒・イエロー)" disabled={color === '2色(黒・イエロー)'}>
                        2色
                        <br />
                        (黒・イエロー)
                    </ToggleButton>
                </ToggleButtonGroup>
            </>
        );
    }
}

export default Color;
