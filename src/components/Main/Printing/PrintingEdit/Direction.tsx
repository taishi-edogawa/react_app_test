import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

interface DirectionInterface {
    direction: string;
}

class Direction extends React.Component<{}, DirectionInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            direction: 'Z字',
        };
    }

    handleDirction = (event: React.MouseEvent, direction: string) => {
        this.setState({ direction });
    };

    render() {
        const direction = this.state.direction;

        return (
            <div>
                <ToggleButtonGroup value={direction} exclusive={true} onChange={this.handleDirction}>
                    <ToggleButton value='Z字' disabled={direction === 'Z字'}>
                        Z字
                    </ToggleButton>
                    <ToggleButton value='N字' disabled={direction === 'N字'}>
                        N字
                    </ToggleButton>
                </ToggleButtonGroup>
            </div>
        );
    }
}

export default Direction;
