import * as React from 'react';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

interface CheckPartitionInterface {
    isDisplayed: boolean;
}

class CheckPartition extends React.Component<{}, CheckPartitionInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            isDisplayed: false
        };
    }

    handleChange = () => {
        this.setState({ isDisplayed: !this.state.isDisplayed });
    };

    private change = () => {
        this.handleChange();
    };

    render() {
        return (
            <>
                <FormControlLabel
                    control={
                        <Switch
                            checked={this.state.isDisplayed}
                            onChange={this.change}
                            value="isDisplayed"
                            color="primary"
                        />
                    }
                    label="仕切り線"
                />
            </>
        );
    }
}

export default CheckPartition;
