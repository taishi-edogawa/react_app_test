import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

interface SurfaceInterface {
    surface: string;
}

class Surface extends React.Component<{}, SurfaceInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            surface: '片面'
        };
    }

    handleDirction = (event: React.MouseEvent, surface: string) => {
        this.setState({ surface });
    };

    render() {
        const surface = this.state.surface;

        return (
            <>
                <ToggleButtonGroup className="flex" value={surface} exclusive={true} onChange={this.handleDirction}>
                    <ToggleButton value="片面" disabled={surface === '片面'}>
                        片面
                    </ToggleButton>
                    <ToggleButton value="両面(左辺綴じ)" disabled={surface === '両面(左辺綴じ)'}>
                        両面
                        <br />
                        (左辺綴じ)
                    </ToggleButton>
                    <ToggleButton value="両面(上辺綴じ)" disabled={surface === '両面(上辺綴じ)'}>
                        両面
                        <br />
                        (上辺綴じ)
                    </ToggleButton>
                    <ToggleButton value="週刊誌綴じ(左ひらき)" disabled={surface === '週刊誌綴じ(左ひらき)'}>
                        週刊誌綴じ
                        <br />
                        (左ひらき)
                    </ToggleButton>
                    <ToggleButton value="週刊誌綴じ(右ひらき)" disabled={surface === '週刊誌綴じ(右ひらき)'}>
                        週刊誌綴じ
                        <br />
                        (右ひらき)
                    </ToggleButton>
                </ToggleButtonGroup>
            </>
        );
    }
}

export default Surface;
