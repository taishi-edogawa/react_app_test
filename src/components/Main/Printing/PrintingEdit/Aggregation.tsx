import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

interface AggregationInterface {
    aggregation: string;
}

class Aggregation extends React.Component<{}, AggregationInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            aggregation: 'しない',
        };
    }

    handleDirction = (event: React.MouseEvent, aggregation: string) => {
        this.setState({ aggregation });
    };

    render() {
        const aggregation = this.state.aggregation;

        return (
            <div>
                <ToggleButtonGroup value={aggregation} exclusive={true} onChange={this.handleDirction}>
                    <ToggleButton value='しない' disabled={aggregation === 'しない'}>
                        しない
                    </ToggleButton>
                    <ToggleButton value='2枚→1枚' disabled={aggregation === '2枚→1枚'}>
                        2枚→1枚
                    </ToggleButton>
                    <ToggleButton value='4枚→1枚' disabled={aggregation === '4枚→1枚'}>
                        4枚→1枚
                    </ToggleButton>
                </ToggleButtonGroup>
            </div>
        );
    }
}

export default Aggregation;
