import * as React from "react";
import useTitle from "components/Main/useTitle";

import Aggregation from "./Aggregation";
import CheckPartition from "./CheckPartition";
import Direction from "./Direction";
import Surface from "./Surface";

interface Props {
    titleChange: (data: string) => void;
}

const printingEdit: React.SFC<Props> = props => {
    useTitle("Edit");
    React.useEffect(() => {
        props.titleChange(document.title);
    });
    return (
        <div>
            <h1>{document.title}</h1>
            <div className="box">
                <p>片面/両面</p>
                <Surface />
            </div>
            <div className="box">
                <p>集約</p>
                <Aggregation />
            </div>
            <div className="box">
                <p>方向</p>
                <Direction />
            </div>
            <div className="box">
                <p>仕切り線</p>
                <CheckPartition />
            </div>
            <p className="annotation">
                選択した項目に応じて、同時選択不可の項目がグレーアウトします。
                <br />
                同時選択不可項目の詳細につきましては、操作マニュアルをご参照ください。
            </p>
        </div>
    );
};

export default printingEdit;
