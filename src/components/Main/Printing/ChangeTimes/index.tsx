import * as React from 'react';

import ChangeTimes from './ChangeTimes';

const chengeTimes: React.SFC = () => (
    <div className="changeTimes">
        <div className="box">
            <p>用紙指定変倍</p>
            <ChangeTimes />
        </div>
    </div>
);

export default chengeTimes;
