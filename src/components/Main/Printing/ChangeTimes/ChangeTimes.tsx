import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

interface ChangeTimesInterface {
    size: string;
}

class ChangeTimes extends React.Component<{}, ChangeTimesInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            size: 'A3'
        };
    }

    handleDirction = (event: React.MouseEvent, size: string) => {
        this.setState({ size });
    };

    render() {
        const size = this.state.size;

        return (
            <>
                <ToggleButtonGroup value={size} exclusive={true} onChange={this.handleDirction}>
                    <ToggleButton value="A3" disabled={size === 'A3'}>
                        A3
                    </ToggleButton>
                    <ToggleButton value="B3" disabled={size === 'B3'}>
                        B3
                    </ToggleButton>
                    <ToggleButton value="A4" disabled={size === 'A4'}>
                        A4
                    </ToggleButton>
                    <ToggleButton value="B4" disabled={size === 'B4'}>
                        B4
                    </ToggleButton>
                </ToggleButtonGroup>
            </>
        );
    }
}

export default ChangeTimes;
