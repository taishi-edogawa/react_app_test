import * as React from "react";
import { initialItems, InitialItems, SetVolume } from "components/Main/initialItems";
import Aggregation from "./Printing/PrintingEdit/Aggregation";

interface Exercise extends InitialItems {
    inId: number;
    setVolume: SetVolume[];
}

interface BodyData {
    bodyWeight: string;
    fatPercentage: string;
}

interface DateElm {
    _now: Date;
    now: Date;
    year: number;
    month: number;
    date: number;
    dayOfWeekStr: string;
}

interface MasterData {
    id: number;
    dateElm: DateElm;
    bodyData: BodyData;
    exc: Exercise[];
}

const test: React.SFC = () => {
    const [initialItem] = React.useState(initialItems);

    const getDate = {
        _now: new Date(),
        get now() {
            return this._now;
        },
        set now(x: Date) {
            this._now = x;
        },
        year() {
            return this.now.getFullYear();
        },
        month() {
            return this.now.getMonth() + 1;
        },
        date() {
            return this.now.getDate();
        },
        dayOfWeekStr() {
            return ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"][this.now.getDay()];
        },
    };

    const initialDate = {
        _now: new Date(),
        get now() {
            return this._now;
        },
        set now(x: Date) {
            this._now = x;
        },
    };

    const dentialDate = {
        year: () => initialDate.now.getFullYear(),
    };

    const dateElm: DateElm = {
        _now: getDate._now,
        now: getDate.now,
        year: getDate.year(),
        month: getDate.month(),
        date: getDate.date(),
        dayOfWeekStr: getDate.dayOfWeekStr(),
    };

    const [stateId, setId] = React.useState(0);
    const [items, setItems] = React.useState(initialItem);
    const [masterData, setMasterData] = React.useState<MasterData[]>([
        {
            dateElm,
            id: stateId,
            bodyData: { bodyWeight: "", fatPercentage: "" },
            exc: [],
        },
    ]);
    const [modalClass, setModalClass] = React.useState("hidden");
    const [flg, toggleFlg] = React.useState<boolean>(false);
    const getId: () => number = () => {
        const idList = [];
        for (const i in masterData) {
            idList.push(masterData[i].id);
        }
        const id = Math.max.apply(null, idList) + 1;
        return id;
    };
    let filterItems = initialItem;

    React.useEffect(() => {
        getDomID();
    });

    let search: HTMLInputElement;
    let selectColors: HTMLInputElement;
    let selectType: HTMLInputElement;
    const getDomID = () => {
        search = document.getElementById("search") as HTMLInputElement;
        selectColors = document.getElementById("selectColors") as HTMLInputElement;
        selectType = document.getElementById("selectType") as HTMLInputElement;
    };
    const addList = () => {
        const updateId = getId();
        const updateList = masterData.concat([]);
        updateList.push({
            dateElm,
            id: updateId,
            bodyData: { bodyWeight: "", fatPercentage: "" },
            exc: [],
        });
        setMasterData(updateList);
        setId(updateId);
        console.log(masterData);
    };

    const firstFilter = () => {
        if (!selectColors.value && !selectType.value) {
            setItems(initialItem);
        } else {
            let updateList: InitialItems[] = [];
            if (selectColors.value && selectType.value) {
                updateList = initialItem.filter(item => {
                    return (
                        item.color.toLowerCase().search(selectColors.value.toLowerCase()) !== -1 &&
                        item.type.toLowerCase().search(selectType.value.toLowerCase()) !== -1
                    );
                });
            } else if (selectColors.value && !selectType.value) {
                updateList = initialItem.filter(item => {
                    return item.color.toLowerCase().search(selectColors.value.toLowerCase()) !== -1;
                });
            } else if (!selectColors.value && selectType.value) {
                updateList = initialItem.filter(item => {
                    return item.type.toLowerCase().search(selectType.value.toLowerCase()) !== -1;
                });
            }
            setItems(updateList);
            filterItems = updateList;
        }
    };

    const filtered = () => {
        firstFilter();
        if (selectColors.value === "clear") {
            const updateList = initialItem.filter(item => {
                return item.name.toLowerCase().search(search.value.toLowerCase()) !== -1;
            });
            setItems(updateList);
        } else {
            const updateList = filterItems.filter(item => {
                return item.name.toLowerCase().search(search.value.toLowerCase()) !== -1;
            });
            setItems(updateList);
        }
    };

    const submit = (ev: React.MouseEvent) => {
        const btn = ev.target as HTMLButtonElement;
        if (btn.value) {
            const updateList = initialItem.filter(item => {
                return item.name.toLowerCase().search(btn.value.toLowerCase()) !== -1;
            });
            const inIdList: number[] = [];
            for (const i in masterData) {
                if (masterData[i].id === stateId) {
                    for (const j in masterData[i].exc) {
                        inIdList.push(Number(masterData[i].exc[j].inId));
                    }
                    let maxInId: number = 0;
                    if (inIdList.length > 0) {
                        maxInId = Math.max.apply(null, inIdList);
                    }
                    updateList[0].inId = maxInId + 1;
                    updateList[0].setVolume = [];
                    updateList[0].setVolume[0] = {
                        weight: "",
                        rep: "",
                        set: "",
                    };

                    // submitItemの中身とupdateListをマージし、重複アイテムは登録しない
                    const result: any[] = [...masterData[i].exc, ...updateList].filter(
                        (item, index, self) => self.findIndex(s => item.name === s.name) === index
                    );
                    // マージした物でsubmitItemを上書き
                    const cloneArr = masterData.concat([]);
                    cloneArr[i].exc = result;
                    setMasterData(cloneArr);
                }
            }

            toggleFlg(true);
        } else {
            toggleFlg(false);
        }
    };

    // const reduction = (ev: React.MouseEvent) => {
    //     const button = ev.target as HTMLButtonElement;
    //     const updateList = Object.assign({}, masterData);
    //     updateList[stateId].exc = masterData[stateId].exc.filter(item => {
    //         return item.inId.toLowerCase().search(button.value.toLowerCase()) === -1;
    //     });
    //     setMasterData(updateList);
    //     let inIdCount = 0;
    //     for (const i in masterData[stateId].exc) {
    //         if (masterData[stateId].exc[i].inId) {
    //             inIdCount += 1;
    //         }
    //     }
    //     if (inIdCount === 0) {
    //         toggleFlg(false);
    //     }
    // };

    // const addSet = (ev: React.MouseEvent) => {
    //     const button = ev.target as HTMLButtonElement;
    //     const inId = button.value;
    //     const plusMinus = button.getAttribute("data-str");
    //     const updateList = Object.assign({}, masterData);
    //     for (const i in updateList[stateId].exc) {
    //         if (updateList[stateId].exc[i].inId === inId) {
    //             if (plusMinus === "plus") {
    //                 if (updateList[stateId].exc[i].setCount < 5) {
    //                     updateList[stateId].exc[i].setCount += 1;
    //                 }
    //             } else {
    //                 if (updateList[stateId].exc[i].setCount > 1) {
    //                     updateList[stateId].exc[i].setCount -= 1;
    //                 }
    //             }
    //         }
    //     }
    //     setMasterData(updateList);
    // };

    // const inputData = (ev: React.ChangeEvent) => {
    //     const input = ev.target as HTMLInputElement;
    //     const inId = input.getAttribute("data-id");
    //     const numType = input.getAttribute("name");
    //     const updateList = Object.assign({}, masterData);
    //     for (const i in updateList[stateId].exc) {
    //         if (updateList[stateId].exc[i].inId === inId) {
    //             if (numType === "weight") {
    //                 updateList[stateId].exc[i].setVolume[0].weight = input.value;
    //             } else if (numType === "rep") {
    //                 updateList[stateId].exc[i].setVolume[0].rep = input.value;
    //             } else if (numType === "set") {
    //                 updateList[stateId].exc[i].setVolume[0].set = input.value;
    //             }
    //         }
    //     }
    //     setMasterData(updateList);
    // };

    const inputFocus = (ev: React.ChangeEvent) => {
        const input = ev.target as HTMLInputElement;
        input.select();
    };

    const save = () => {
        // このsaveで保存されたjsonかどうかを識別するためのコード埋め込み
        masterData[99999999999999999] = {
            dateElm,
            id: 99999999999999999,
            bodyData: { bodyWeight: "", fatPercentage: "" },
            exc: [],
        };
        const data = JSON.stringify(masterData);
        const a = document.createElement("a");
        a.href = `data:text/plain,${encodeURIComponent(data)}`;
        a.download = "test.json";
        a.click();
    };

    const load = () => {
        const upload: HTMLInputElement = document.getElementById("upload") as HTMLInputElement;
        upload.click();
    };

    const upload = () => {
        const upload: HTMLInputElement = document.getElementById("upload") as HTMLInputElement;
        const file = upload.files;
        if (file && file.length) {
            const reader = new FileReader();
            reader.readAsText(file[0]);
            reader.onload = () => {
                const data = JSON.parse(String(reader.result));
                let identifiFlg = false;
                for (const i in data) {
                    // このアプリのsaveコマンドでsaveされたjsonかどうか識別
                    if (data[i].date === "identificationXXXX") {
                        delete data[i];
                        identifiFlg = true;
                        toggleFlg(true);
                        setMasterData(data);
                    }
                }
                if (identifiFlg) {
                    console.log("flg");
                } else {
                    console.log("desFlg");
                }
            };
        }
    };

    const currentBox = (ev: any) => {
        const currentId = Number(ev.currentTarget.getAttribute("data-id"));
        setId(currentId);
    };

    const dataInput = (ev: React.ChangeEvent) => {
        const input = ev.target as HTMLInputElement;
        const currentId = Number(input.getAttribute("data-id"));
        const numType = input.getAttribute("name");
        const updateList = masterData.concat([]);

        for (const i in updateList) {
            if (updateList[i].id === currentId) {
                if (numType === "bodyWeight") {
                    updateList[i].bodyData.bodyWeight = input.value;
                } else if (numType === "fatPercentage") {
                    updateList[i].bodyData.fatPercentage = input.value;
                }
                setMasterData(updateList);
            }
        }
    };

    // bindする書き方
    const deleteBox = (event: Event | undefined) => {
        return (ev: React.MouseEvent) => {
            const button = ev.target as HTMLButtonElement;
            const currentId = Number(button.getAttribute("data-id"));
            const updateList = masterData.concat([]);
            const idList = [];
            for (const i in updateList) {
                if (updateList[i].id === currentId) {
                    delete updateList[i];
                } else {
                    idList.push(updateList[i].id);
                }
            }
            const maxId: number = Math.max.apply(null, idList);
            setId(maxId);
            const cloneArr = updateList.filter(v => v);
            setMasterData(cloneArr);
            // ここのeventコールに意味はない...
            // deleteBoxを発火する際bindしたいのでevent => deleteBox(event)って書きたいのだが
            // JSX-no-ramdaに引っかかってしまうためこんな書き方してる
            event;
        };
    };

    const addExercise = () => {
        setModalClass("");
    };

    const modalHide = () => {
        setModalClass("hidden");
    };

    const propagationStop = (ev: React.MouseEvent<HTMLFormElement, MouseEvent>) => {
        ev.stopPropagation();
    };

    const da = () => {};

    const openDetail = () => {};

    const newList = [];
    let currentFlg = false;
    for (const i in masterData) {
        let currentClass = "";
        let hiddenClass = "hidden";
        if (masterData[i].id === stateId) {
            currentClass = "current";
            currentFlg = true;
        }
        if (masterData.length > 1) {
            hiddenClass = "";
        }
        // currentClass = "current"にならないままループが終わる場合最後のループでcurrentを付与
        if (Number(i) + 1 === masterData.length && !currentFlg) {
            currentClass = "current";
        }
        const exerciseList = [];
        if (masterData[i].exc) {
            for (const j in masterData[i].exc) {
                const setVolumeList = [];
                for (const k in masterData[i].exc[j].setVolume) {
                    setVolumeList.push(
                        <>
                            <dl key={j}>
                                <dd>
                                    <input
                                        type="number"
                                        step="0.25"
                                        name="weight"
                                        data-id={masterData[i].id}
                                        data-inId={masterData[i].exc[j].inId}
                                        value={masterData[i].exc[j].setVolume[k].weight}
                                        onChange={dataInput}
                                    />
                                    <br />
                                    <span>kg</span>
                                </dd>
                                <dd>
                                    <input
                                        type="number"
                                        step="1"
                                        name="rep"
                                        data-id={masterData[i].id}
                                        data-inId={masterData[i].exc[j].inId}
                                        value={masterData[i].exc[j].setVolume[k].rep}
                                        onChange={dataInput}
                                    />
                                    <br />
                                    <span>回</span>
                                </dd>
                                <dd>
                                    <input
                                        type="number"
                                        step="1"
                                        name="set"
                                        data-id={masterData[i].id}
                                        data-inId={masterData[i].exc[j].inId}
                                        value={masterData[i].exc[j].setVolume[k].set}
                                        onChange={dataInput}
                                    />
                                    <br />
                                    <span>セット</span>
                                </dd>
                            </dl>
                        </>
                    );
                }

                exerciseList.push(
                    <div className="exerciseBox" key={i}>
                        <p className="title">{masterData[i].exc[j].name}</p>
                        {setVolumeList}
                    </div>
                );
            }
        }

        newList.push(
            <div
                key={i}
                className={`${currentClass} dataBox`}
                data-id={masterData[i].id}
                onClick={currentBox}
                onMouseEnter={currentBox}
            >
                <dl>
                    <dt>体重</dt>
                    <dd>
                        <input
                            type="number"
                            step="0.01"
                            name="bodyWeight"
                            data-id={masterData[i].id}
                            value={masterData[i].bodyData.bodyWeight}
                            onChange={dataInput}
                        />
                        kg
                    </dd>
                </dl>
                <dl>
                    <dt>体脂肪率</dt>
                    <dd>
                        <input
                            type="number"
                            step="0.1"
                            name="fatPercentage"
                            data-id={masterData[i].id}
                            value={masterData[i].bodyData.fatPercentage}
                            onChange={dataInput}
                        />
                        %
                    </dd>
                </dl>
                {exerciseList}
                <div className="btnWrap">
                    <button
                        className="add"
                        onClick={addExercise}
                        data-id={masterData[i].id}
                        name="add"
                    >
                        <i className="material-icons">add</i>種目を追加する
                    </button>
                    <button
                        className={`${hiddenClass} delete`}
                        onClick={deleteBox(event)}
                        data-id={masterData[i].id}
                        name="delete"
                    >
                        <i className="material-icons">delete_forever</i>
                        このBOXを削除する
                    </button>
                    <button
                        className="openDetail"
                        onClick={openDetail}
                        data-id={masterData[i].id}
                        name="openDetail"
                    >
                        <i className="material-icons">list_alt</i>
                        詳細を表示する
                    </button>
                </div>
            </div>
        );
    }

    const list = [];
    for (const i in items) {
        list.push(
            <li key={i}>
                <button value={items[i].name} onClick={submit} type="button">
                    {items[i].name}
                </button>
            </li>
        );
    }

    let colorListOld = [];
    let typeListOld = [];
    const colorList: JSX.Element[] = [];
    const typeList: JSX.Element[] = [];
    // initialItemからkey:colorとkey:typeの文字列を配列に入れる
    for (const i in initialItem) {
        colorListOld.push(initialItem[i].color);
        typeListOld.push(initialItem[i].type);
    }
    // colorListOldとtypeListOldからダブっている名前を削除
    colorListOld = colorListOld.filter((item, index, self) => {
        return self.indexOf(item) === index;
    });
    typeListOld = typeListOld.filter((item, index, self) => {
        return self.indexOf(item) === index;
    });

    // ダブりを削除した配列からElementの配列を作成
    for (const i in colorListOld) {
        colorList.push(
            <option key={i} value={colorListOld[i]}>
                {colorListOld[i]}
            </option>
        );
    }
    for (const i in typeListOld) {
        typeList.push(
            <option key={i} value={typeListOld[i]}>
                {typeListOld[i]}
            </option>
        );
    }

    // const excList = [];
    // for (const i in masterData[stateId].exc) {
    //     const countList = [];
    //     const name = masterData[stateId].exc[i].name;
    //     const inId = masterData[stateId].exc[i].inId;
    //     const weight = masterData[stateId].exc[i].setVolume.weight;
    //     const rep = masterData[stateId].exc[i].setVolume.rep;
    //     const set = masterData[stateId].exc[i].setVolume.set;
    //     for (let j = 0; j < masterData[stateId].exc[i].setCount; j += 1) {
    //         countList.push(
    //             <div key={j}>
    //                 <div>
    //                     重量
    //                     <input
    //                         type="number"
    //                         name="weight"
    //                         data-id={inId}
    //                         onChange={inputData}
    //                         onFocus={inputFocus}
    //                         defaultValue={weight}
    //                     />
    //                     kg
    //                 </div>
    //                 <div>
    //                     <input
    //                         type="number"
    //                         name="rep"
    //                         data-id={inId}
    //                         onChange={inputData}
    //                         onFocus={inputFocus}
    //                         defaultValue={rep}
    //                     />
    //                     個
    //                     <input
    //                         type="number"
    //                         name="set"
    //                         data-id={inId}
    //                         onChange={inputData}
    //                         onFocus={inputFocus}
    //                         defaultValue={set}
    //                     />
    //                     箱
    //                 </div>
    //             </div>
    //         );
    //     }

    //     excList.push(
    //         <li key={i}>
    //             <h3>{name}</h3>
    //             <select defaultValue="0">
    //                 <option value="0">春</option>
    //                 <option value="1">夏</option>
    //                 <option value="2">秋</option>
    //                 <option value="3">冬</option>
    //             </select>
    //             {countList}
    //             <button onClick={addSet} value={inId} data-str="plus">
    //                 +
    //             </button>
    //             <button onClick={addSet} value={inId} data-str="minus">
    //                 -
    //             </button>
    //             <button onClick={reduction} value={inId}>
    //                 削除
    //             </button>
    //         </li>
    //     );
    // }

    return (
        <div>
            <header>
                <ul className="clearfix">
                    <li>
                        <button name="add" onClick={addList}>
                            <i className="material-icons">add</i>
                        </button>
                    </li>
                    <li>
                        <button name="save" onClick={save}>
                            Save
                        </button>
                    </li>
                    <li className="load">
                        <button name="load" onClick={load}>
                            Load
                        </button>
                        <input type="file" id="upload" onChange={upload} />
                    </li>
                </ul>
            </header>
            <main id="main">{newList}</main>

            <aside className={`${modalClass} modal`} onClick={modalHide}>
                <form action="" className="modalInner" onClick={propagationStop}>
                    <p>name</p>
                    <input id="search" type="text" placeholder="search" onChange={filtered} />
                    <p>color</p>
                    <select onChange={filtered} id="selectColors">
                        <option value="">colorFilter無し</option>
                        {colorList}
                    </select>
                    <select onChange={filtered} id="selectType">
                        <option value="">infanalFilter無し</option>
                        {typeList}
                    </select>
                    <ul>{list}</ul>
                    {flg && (
                        <>
                            {/* <ul>{excList}</ul> */}
                            <button name="save" onClick={save}>
                                この状態を保存する
                            </button>
                        </>
                    )}
                </form>
            </aside>
        </div>
    );
};

export default test;
