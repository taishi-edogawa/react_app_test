import * as React from 'react';

interface CenterAndFrameProps {
    check?: string;
}

class CenterAndFrame extends React.Component<CenterAndFrameProps> {
    render() {
        const check = this.props.check;
        if (check !== 'センター・枠消去') return null;
        return (
            <>
                <p>センター・枠消去コンポーネント</p>
            </>
        );
    }
}

export default CenterAndFrame;
