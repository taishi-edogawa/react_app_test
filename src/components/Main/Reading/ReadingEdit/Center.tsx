import * as React from 'react';

interface CenterProps {
    check?: string;
}

class Center extends React.Component<CenterProps> {
    render() {
        const check = this.props.check;
        if (check !== 'センター消去') return null;
        return (
            <>
                <p>センター消去コンポーネント</p>
            </>
        );
    }
}

export default Center;
