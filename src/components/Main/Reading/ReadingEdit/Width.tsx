import * as React from 'react';

import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

interface WidthProps {
    check?: string;
}

interface WidthInterface {
    value: string;
}

interface ElementEvent<T extends HTMLElement> extends Event {
    target: T;
}

class Width extends React.Component<WidthProps, WidthInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            value: '同一幅',
        };
    }

    handleChange = (event: ElementEvent<HTMLInputElement>) => {
        this.setState({ value: event.target.value });
    };

    private change = (event: any) => {
        this.handleChange(event);
    };

    render() {
        const check = this.props.check;
        const checkBool = check === '消去しない' || check === 'センター消去' ? true : false;
        return (
            <div>
                <FormControl>
                    <RadioGroup name='type' value={this.state.value} onChange={this.change}>
                        <FormControlLabel value='同一幅' control={<Radio disabled={checkBool} />} label='同一幅' />
                        <FormControlLabel value='個別指定' control={<Radio disabled={checkBool} />} label='個別指定' />
                    </RadioGroup>
                </FormControl>
            </div>
        );
    }
}

export default Width;
