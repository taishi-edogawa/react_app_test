import * as React from 'react';

import Button from '@material-ui/core/Button';
import ExposurePlus1 from '@material-ui/icons/ExposurePlus1';
import ExposureNeg1 from '@material-ui/icons/ExposureNeg1';
import Fab from '@material-ui/core/Fab';
import { createStyles, withStyles, WithStyles, Theme } from '@material-ui/core/styles';

interface AdjustmentInterface {
    count: number;
}

interface AdjustmentProps {
    check: string;
}

const styles = () =>
    createStyles({
        count: {
            fontSize: '1.5rem',
            fontWeight: 'bold',
            textAlign: 'center',
            width: 48,
        },
        adjustConfig: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-end',
        },
        configButton: {
            // width: 48,
        },
    });

export interface Props extends WithStyles<typeof styles>, AdjustmentProps {
    theme: Theme;
}

const adjustment = withStyles(styles)(
    class extends React.Component<Props, AdjustmentInterface> {
        state = {
            count: 0,
        };

        increment = () => {
            this.setState({ count: this.state.count + 1 });
        };

        decrement = () => {
            if (this.state.count > 0) {
                this.setState({ count: this.state.count - 1 });
            }
        };

        render() {
            const { classes, check } = this.props;
            const checkBool = check === '消去しない' || check === 'センター消去' ? true : false;

            return (
                <div className={classes.adjustConfig}>
                    <Fab size='medium' area-label='Add' disabled={checkBool}>
                        <ExposureNeg1 onClick={this.decrement} />
                    </Fab>
                    <Fab size='medium' area-label='Add' disabled={checkBool}>
                        <ExposurePlus1 onClick={this.increment} />
                    </Fab>
                    <Button className={classes.configButton}>設定</Button>
                </div>
            );
        }
    },
);

export default withStyles(styles, { withTheme: true })(adjustment);
