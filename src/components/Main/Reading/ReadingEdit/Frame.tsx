import * as React from 'react';

interface FrameProps {
    check?: string;
}

class Frame extends React.Component<FrameProps> {
    render() {
        const check = this.props.check;
        if (check !== '枠消去') return null;
        return (
            <>
                <p>枠消去コンポーネント</p>
            </>
        );
    }
}

export default Frame;
