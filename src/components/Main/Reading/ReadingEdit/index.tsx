import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

import Adjustment from './Adjustment';
import Center from './Center';
import CenterAndFrame from './CenterAndFrame';
import Frame from './Frame';
import Width from './Width';

import override from 'CSSOverride/override';
import { withStyles, WithStyles, Theme } from '@material-ui/core/styles';

interface CheckDeleteInterface {
    check: string;
}

export interface Props extends WithStyles<typeof override> {
    theme: Theme;
}

class ReadingEdit extends React.Component<Props, CheckDeleteInterface> {
    state = {
        check: '消去しない',
    };

    handleDirction = (event: React.MouseEvent, check: string) => {
        this.setState({ check });
    };

    render() {
        const { classes } = this.props;
        const check = this.state.check;

        return (
            <div>
                <div className='box'>
                    <ToggleButtonGroup value={check} exclusive={true} onChange={this.handleDirction}>
                        <ToggleButton value='消去しない' disabled={check === '消去しない'}>
                            消去しない
                        </ToggleButton>
                        <ToggleButton value='枠消去' disabled={check === '枠消去'}>
                            枠消去
                        </ToggleButton>
                        <ToggleButton value='センター消去' disabled={check === 'センター消去'}>
                            センター消去
                        </ToggleButton>
                        <ToggleButton value='センター・枠消去' disabled={check === 'センター・枠消去'}>
                            センター・枠消去
                        </ToggleButton>
                    </ToggleButtonGroup>
                </div>
                <p className={classes.note}>＋、－、または設定ボタンで周囲の消去幅を指定してください。</p>
                <Width check={check} />
                <Frame check={check} />
                <Center check={check} />
                <CenterAndFrame check={check} />
                <Adjustment check={check} />
            </div>
        );
    }
}

export default withStyles(override)(ReadingEdit);
