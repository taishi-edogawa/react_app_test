import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

interface CheckDeleteInterface {
    check: string;
}

class CheckDelete extends React.Component<{}, CheckDeleteInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            check: '消去しない',
        };
    }

    handleDirction = (event: React.MouseEvent, check: string) => {
        this.setState({ check });
    };

    render() {
        const check = this.state.check;

        return (
            <div>
                <ToggleButtonGroup value={check} exclusive={true} onChange={this.handleDirction}>
                    <ToggleButton value='消去しない' disabled={check === '消去しない'}>
                        消去しない
                    </ToggleButton>
                    <ToggleButton value='枠消去' disabled={check === '枠消去'}>
                        枠消去
                    </ToggleButton>
                    <ToggleButton value='センター消去' disabled={check === 'センター消去'}>
                        センター消去
                    </ToggleButton>
                    <ToggleButton value='センター・枠消去' disabled={check === 'センター・枠消去'}>
                        センター・枠消去
                    </ToggleButton>
                </ToggleButtonGroup>
            </div>
        );
    }
}

export default CheckDelete;
