import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

interface ResolutionInterface {
    reso: string;
}

class Resolution extends React.Component<{}, ResolutionInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            reso: '200dpi'
        };
    }

    handleSplit = (event: React.MouseEvent, reso: string) => {
        this.setState({ reso });
    };

    render() {
        const reso = this.state.reso;

        return (
            <div className="box">
                <ToggleButtonGroup value={reso} exclusive={true} onChange={this.handleSplit}>
                    <ToggleButton value="200dpi" disabled={reso === '200dpi'}>
                        200dpi
                    </ToggleButton>
                    <ToggleButton value="300dpi" disabled={reso === '300dpi'}>
                        300dpi
                    </ToggleButton>
                    <ToggleButton value="400dpi" disabled={reso === '400dpi'}>
                        400dpi
                    </ToggleButton>
                    <ToggleButton value="600dpi" disabled={reso === '600dpi'}>
                        600dpi
                    </ToggleButton>
                </ToggleButtonGroup>
            </div>
        );
    }
}

export default Resolution;
