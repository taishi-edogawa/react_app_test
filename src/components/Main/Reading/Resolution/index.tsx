import * as React from 'react';

import Resolution from './Resolution';
import Lock from './Lock';

const resolution: React.SFC = () => (
    <div className="resolution">
        <Resolution />
        <Lock />
    </div>
);

export default resolution;
