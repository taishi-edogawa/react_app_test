import * as React from 'react';

import CheckAuto from './CheckAuto';
import Depth from './Depth';

const depth: React.SFC = () => (
    <div className="depth">
        <CheckAuto />
        <div className="box">
            <p>濃淡</p>
            <Depth />
        </div>
    </div>
);

export default depth;
