import * as React from 'react';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

const styles = createStyles({
    root: {
        background: 'transparent',
        maxWidth: 400,
        flexGrow: 1
    },
    dot: {
        background: 'red'
    }
});

interface Props extends WithStyles<typeof styles> {
    theme: Theme;
}

interface DepthInterface {
    activeStep: number;
}

class Depth extends React.Component<Props, DepthInterface> {
    constructor(props: Props) {
        super(props);
        this.state = {
            activeStep: 3
        };
    }
    handleNext = () => {
        this.setState(state => ({
            activeStep: state.activeStep + 1
        }));
    };

    handleBack = () => {
        this.setState(state => ({
            activeStep: state.activeStep - 1
        }));
    };
    render() {
        const { classes, theme } = this.props;
        return (
            <>
                <MobileStepper
                    variant="dots"
                    steps={7}
                    position="static"
                    activeStep={this.state.activeStep}
                    className={classes.root}
                    nextButton={
                        <Button size="small" onClick={this.handleNext} disabled={this.state.activeStep === 6}>
                            {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                        </Button>
                    }
                    backButton={
                        <Button size="small" onClick={this.handleBack} disabled={this.state.activeStep === 0}>
                            {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                        </Button>
                    }
                />
            </>
        );
    }
}

export default withStyles(styles, { withTheme: true })(Depth);
