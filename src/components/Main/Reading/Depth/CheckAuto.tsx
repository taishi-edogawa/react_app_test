import * as React from 'react';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

interface CheckAutoInterface {
    isAuto: boolean;
}

class CheckAuto extends React.Component<{}, CheckAutoInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            isAuto: false
        };
    }

    handleChange = () => {
        this.setState({ isAuto: !this.state.isAuto });
    };

    private change = () => {
        this.handleChange();
    };

    render() {
        return (
            <div className="box">
                <FormControlLabel
                    className="switch"
                    control={
                        <Switch checked={this.state.isAuto} onChange={this.change} value="isAuto" color="primary" />
                    }
                    label="自動濃度"
                />
            </div>
        );
    }
}

export default CheckAuto;
