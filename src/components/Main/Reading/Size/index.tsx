import * as React from 'react';

import Size from './Size';
import Detection from './Detection';
const size: React.SFC = () => (
    <div className="size">
        <Detection />
        <Size />
    </div>
);

export default size;
