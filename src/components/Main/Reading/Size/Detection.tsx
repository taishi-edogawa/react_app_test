import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

interface SizeInterface {
    size: string;
}

class Detection extends React.Component<{}, SizeInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            size: '自動検知(同一サイズ)'
        };
    }

    handleSplit = (event: React.MouseEvent, size: string) => {
        this.setState({ size });
    };

    render() {
        const size = this.state.size;

        return (
            <div className="box">
                <ToggleButtonGroup value={size} exclusive={true} onChange={this.handleSplit}>
                    <ToggleButton value="自動検知(同一サイズ)" disabled={size === '自動検知(同一サイズ)'}>
                        自動検知(同一サイズ)
                    </ToggleButton>
                    <ToggleButton value="自動検知(混載サイズ)" disabled={size === '自動検知(混載サイズ)'}>
                        自動検知(混載サイズ)
                    </ToggleButton>
                </ToggleButtonGroup>
            </div>
        );
    }
}

export default Detection;
