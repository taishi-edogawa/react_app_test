import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import Icon from '@material-ui/core/Icon';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';

const styles = createStyles({
    box: {
        marginBottom: '0.5em'
    }
});

export interface Props extends WithStyles<typeof styles> {}

interface SizeInterface {
    size: string;
}

class Size extends React.Component<Props, SizeInterface> {
    constructor(props: Props) {
        super(props);
        this.state = {
            size: 'A2L'
        };
    }

    handleSplit = (event: React.MouseEvent, size: string) => {
        this.setState({ size });
    };

    render() {
        const { classes } = this.props;
        const size = this.state.size;

        return (
            <>
                <div className={`box ${classes.box}`}>
                    <ToggleButtonGroup value={size} exclusive={true} onChange={this.handleSplit}>
                        <ToggleButton value="A2L" disabled={size === 'A2L'}>
                            A2<Icon className="icon crop_l">crop_16_9</Icon>
                        </ToggleButton>
                        <ToggleButton value="B3L" disabled={size === 'B3L'}>
                            B3<Icon className="icon crop_l">crop_16_9</Icon>
                        </ToggleButton>
                        <ToggleButton value="A3P" disabled={size === 'A3P'}>
                            A3<Icon className="icon crop_p">crop_portrait</Icon>
                        </ToggleButton>
                        <ToggleButton value="B4P" disabled={size === 'B4P'}>
                            B4<Icon className="icon crop_p">crop_portrait</Icon>
                        </ToggleButton>
                    </ToggleButtonGroup>
                </div>
                <div className={`box ${classes.box}`}>
                    <ToggleButtonGroup value={size} exclusive={true} onChange={this.handleSplit}>
                        <ToggleButton value="A3L" disabled={size === 'A3L'}>
                            A3<Icon className="icon crop_l">crop_16_9</Icon>
                        </ToggleButton>
                        <ToggleButton value="B4L" disabled={size === 'B4L'}>
                            B4<Icon className="icon crop_l">crop_16_9</Icon>
                        </ToggleButton>
                        <ToggleButton value="A4P" disabled={size === 'A4P'}>
                            A4<Icon className="icon crop_p">crop_portrait</Icon>
                        </ToggleButton>
                        <ToggleButton value="A4L" disabled={size === 'A4L'}>
                            A4<Icon className="icon crop_l">crop_16_9</Icon>
                        </ToggleButton>
                        <ToggleButton value="B5P" disabled={size === 'B5P'}>
                            B5<Icon className="icon crop_p">crop_portrait</Icon>
                        </ToggleButton>
                        <ToggleButton value="B5L" disabled={size === 'B5L'}>
                            B5<Icon className="icon crop_l">crop_16_9</Icon>
                        </ToggleButton>
                    </ToggleButtonGroup>
                </div>
                <div className={`box ${classes.box}`}>
                    <ToggleButtonGroup value={size} exclusive={true} onChange={this.handleSplit}>
                        <ToggleButton value="名刺" disabled={size === '名刺'}>
                            名刺<Icon className="icon crop_l">crop_16_9</Icon>
                        </ToggleButton>
                    </ToggleButtonGroup>
                </div>
            </>
        );
    }
}

export default withStyles(styles)(Size);
