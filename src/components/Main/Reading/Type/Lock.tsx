import * as React from 'react';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

interface LockInterface {
    isLock: boolean;
}

class Lock extends React.Component<{}, LockInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            isLock: false
        };
    }

    handleChange = () => {
        this.setState({ isLock: !this.state.isLock });
    };

    private change = () => {
        this.handleChange();
    };

    render() {
        return (
            <div className="box alignRight">
                <FormControlLabel
                    className="switch"
                    control={
                        <Switch checked={this.state.isLock} onChange={this.change} value="isLock" color="primary" />
                    }
                    label="変更ロック"
                />
            </div>
        );
    }
}

export default Lock;
