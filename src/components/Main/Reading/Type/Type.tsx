import * as React from 'react';

import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

interface LockInterface {
    value: string;
}

class Type extends React.Component<{}, LockInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            value: '白黒：文字',
        };
    }

    handleChange = (event: React.FormEvent<HTMLInputElement>) => {
        this.setState({ value: event.currentTarget.value });
    };

    private change = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.handleChange(event);
    };

    render() {
        return (
            <div className='box'>
                <FormControl>
                    <RadioGroup name='type' value={this.state.value} onChange={this.change} className='radioGroup'>
                        <FormControlLabel value='白黒：文字' control={<Radio />} label='白黒：文字' />
                        <FormControlLabel value='白黒：文字・図表' control={<Radio />} label='白黒：文字・図表' />
                        <FormControlLabel value='白黒：文字・写真' control={<Radio />} label='白黒：文字・写真' />
                        <FormControlLabel value='白黒：写真' control={<Radio />} label='白黒：写真' />
                        <FormControlLabel
                            className='w100'
                            value='グレースケール'
                            control={<Radio />}
                            label='グレースケール'
                        />
                    </RadioGroup>
                    <RadioGroup name='type' value={this.state.value} onChange={this.change} className='radioGroup'>
                        <FormControlLabel
                            value='フルカラー：文字・写真'
                            control={<Radio />}
                            label='フルカラー：文字・写真'
                        />
                        <FormControlLabel
                            value='フルカラー：印画紙写真'
                            control={<Radio />}
                            label='フルカラー：印画紙写真'
                        />
                        <FormControlLabel
                            className='w100'
                            value='自動カラー選択'
                            control={<Radio />}
                            label='自動カラー選択'
                        />
                    </RadioGroup>
                </FormControl>
            </div>
        );
    }
}

export default Type;
