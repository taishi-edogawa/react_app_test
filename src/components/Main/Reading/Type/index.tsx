import * as React from 'react';

import Lock from './Lock';
import Type from './Type';

const type: React.SFC = () => (
    <div className="type">
        <Type />
        <Lock />
    </div>
);

export default type;
