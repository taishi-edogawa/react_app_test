import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

interface SplitInterface {
    feed: string;
}

class Split extends React.Component<{}, SplitInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            feed: 'いいえ',
        };
    }

    handleSplit = (event: React.MouseEvent, feed: string) => {
        this.setState({ feed });
    };

    render() {
        const feed = this.state.feed;
        const arr = ['いいえ', '大量原稿', 'SADF'];

        return (
            <div>
                <ToggleButtonGroup value={feed} exclusive={true} onChange={this.handleSplit}>
                {arr.map(key => (
                    <ToggleButton key={key} value={key} disabled={feed === key}>{key}</ToggleButton>
                ))}
                </ToggleButtonGroup>
            </div>
        );
    }
}

export default Split;
