import * as React from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

interface SurfaceInterface {
    manuscript: string;
}

class Surface extends React.Component<{}, SurfaceInterface> {
    constructor(props: {}) {
        super(props);
        this.state = {
            manuscript: '片面',
        };
    }

    handleManuscript = (event: React.MouseEvent, manuscript: string) => {
        this.setState({ manuscript });
    };

    render() {
        const manuscript = this.state.manuscript;
        const arr = ['片面', '左右ひらき', '両面：上下ひらき'];

        return (
            <div>
                <ToggleButtonGroup value={manuscript} exclusive={true} onChange={this.handleManuscript}>
                {arr.map(key => (
                    <ToggleButton key={key} value={key} disabled={manuscript === key}>{key}</ToggleButton>
                ))}
                </ToggleButtonGroup>
            </div>
        );
    }
}

export default Surface;
