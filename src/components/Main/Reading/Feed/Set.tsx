import * as React from 'react';

import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

interface SetInterface {
    direction: string;
}

const styles = (theme: Theme) =>
    createStyles({
        selected: {
            backgroundColor: theme.palette.secondary.main,
        },
        disabled: { backgroundColor: theme.palette.secondary.main },
    });

export interface Props extends WithStyles<typeof styles> {}

class Set extends React.Component<Props, SetInterface> {
    constructor(props: Props) {
        super(props);
        this.state = {
            direction: '横',
        };
    }

    handleDirction = (event: React.MouseEvent, direction: string) => {
        this.setState({ direction });
    };

    render() {
        const { classes } = this.props;
        const direction = this.state.direction;

        return (
            <>
                <ToggleButtonGroup value={direction} exclusive={true} onChange={this.handleDirction}>
                    <ToggleButton
                        classes={{ selected: classes.selected, disabled: classes.disabled }}
                        value='横'
                        disabled={direction === '横'}
                    >
                        横向き
                    </ToggleButton>
                    <ToggleButton
                        classes={{ selected: classes.selected, disabled: classes.disabled }}
                        value='縦'
                        disabled={direction === '縦'}
                    >
                        縦向き
                    </ToggleButton>
                </ToggleButtonGroup>
            </>
        );
    }
}

export default withStyles(styles)(Set);
