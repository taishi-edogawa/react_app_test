import * as React from 'react';

import Set from './Set';
import Split from './Split';
import Surface from './Surface';

const feed: React.SFC = () => (
    <div className='feed'>
        <p>原稿セット方向</p>
        <Set />
        <div className='box'>
            <p>原稿面</p>
            <Surface />
        </div>
        <div className='box'>
            <p>分割大量給紙</p>
            <Split />
        </div>
    </div>
);

export default feed;
