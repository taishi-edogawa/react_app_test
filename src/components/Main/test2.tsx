import * as React from "react";
import { initialFoods as foods, InitialFoods as Foods } from "components/Main/initialFoods";

interface FoodsData {
    id: number;
    name: string;
    referenceWeight: number;
    protein: number;
    fat: number;
    carbo: number;
    cal: number;
}

interface Page {
    pageId: number;
    title: string;
    setList: FoodsData[];
}

const test: React.SFC = () => {
    const [initialFoods] = React.useState(foods);
    const [stateFoods, setStateFoods] = React.useState<FoodsData[]>([
        {
            id: 0,
            name: "食品を選択して下さい",
            referenceWeight: 100,
            protein: 0,
            fat: 0,
            carbo: 0,
            cal: 0,
        },
    ]);
    const [statePage, setStatePage] = React.useState([
        {
            pageId: 0,
            get title() {
                return `セット${this.pageId}`;
            },
            setList: stateFoods,
        },
        {
            pageId: 1,
            get title() {
                return `セット${this.pageId}`;
            },
            setList: stateFoods,
        },
    ]);
    const [statePageIndex, setStatePageIndex] = React.useState(0);
    const [stateCategory, setStateCategory] = React.useState();
    const [stateBodyWeight, setStateBodyWeight] = React.useState("");

    const getId: () => number = () => {
        const idList = [];
        for (const i in stateFoods) {
            idList.push(stateFoods[i].id);
        }
        const id = Math.max.apply(null, idList) + 1;
        return id;
    };

    const importFoodData = () => {
        const localFoodsData = localStorage.getItem("foods")!;
        if (localFoodsData) {
            const foodsData = JSON.parse(localFoodsData);
            setStateFoods(foodsData);
            calcFoodsTotalPFC(foodsData);
            // renderが完了していないとDOMがnullになるため遅延させているが納得いかない、useEffectでもうまくいかないし...
            setTimeout(() => {
                for (const i in foodsData) {
                    calcFoodPFC(foodsData[i].id, foodsData[i].name, foodsData[i].referenceWeight);
                }
            }, 1000);
        }
    };

    const importBodyWeight = () => {
        const localBodyWeightData = localStorage.getItem("bodyWeight");
        if (localBodyWeightData) {
            setStateBodyWeight(localBodyWeightData);
            calcPFC(Number(localBodyWeightData));
        }
    };

    // 空配列を渡してマウントとアンマウント時のみ実行させている
    React.useEffect(() => {
        importBodyWeight();
        importFoodData();
    }, []);

    const inputFocus = (ev: React.ChangeEvent) => {
        const input = ev.target as HTMLInputElement;
        input.select();
    };

    const propagationStop = (ev: React.MouseEvent<HTMLFormElement, MouseEvent>) => {
        ev.stopPropagation();
    };

    class FoodDataSearch {
        name: string;
        index: number;
        constructor(name: string) {
            this.name = name;
            this.index = 0;
        }
        get data() {
            for (const i in initialFoods) {
                if (initialFoods[i].name.indexOf(this.name) >= 0) {
                    this.index = Number(i);
                }
            }
            return initialFoods[this.index];
        }
    }

    const calcPFC = (weight: number) => {
        const totalP = document.querySelector("#totalP span") as HTMLSpanElement;
        const totalF = document.querySelector("#totalF span") as HTMLSpanElement;
        const totalC = document.querySelector("#totalC span") as HTMLSpanElement;
        const totalCal = document.querySelector("#totalCal span") as HTMLSpanElement;
        // 標準的な体脂肪率を18%とし、除脂肪体重は体重*0.82としている
        const leanWeight = weight * 0.82;
        const p = leanWeight * 3;
        const f = weight * 0.6;
        const c = weight * 7;
        const cal = p * 4 + f * 9 + c * 4;
        totalP.textContent = String(Math.round(p * 10) / 10);
        totalF.textContent = String(Math.round(f * 10) / 10);
        totalC.textContent = String(Math.round(c * 10) / 10);
        totalCal.textContent = String(Math.round(cal * 10) / 10);
    };

    const inputBodyWeight = (ev: React.ChangeEvent) => {
        const input = ev.target as HTMLInputElement;
        const weight = input.value;
        saveWeight(weight);
        setStateBodyWeight(weight);
        calcPFC(Number(weight));
    };

    const finalize = (data: number, rate: number) => {
        return Math.round(data * rate * 10) / 10;
    };

    // 数字のみtrue, それ以外とNaNやinfiniteもfalse
    const isNumber = (v: any) => {
        return typeof v === "number" && isFinite(v);
    };

    const getTotalData = (arr: FoodsData[], param: string) => {
        let totalData: number = 0;
        for (const i in arr) {
            if (arr[i][param] && isNumber(arr[i][param])) {
                totalData += arr[i][param];
            } else {
                totalData += 0;
            }
        }
        return Math.round(totalData * 10) / 10;
    };

    const calcFoodsTotalPFC = (arr: FoodsData[]) => {
        const totalfoodP = document.querySelector("#totalFoodP span") as HTMLSpanElement;
        const totalfoodF = document.querySelector("#totalFoodF span") as HTMLSpanElement;
        const totalfoodC = document.querySelector("#totalFoodC span") as HTMLSpanElement;
        const totalfoodCal = document.querySelector("#totalFoodCal span") as HTMLSpanElement;
        totalfoodP.textContent = String(getTotalData(arr, "protein"));
        totalfoodF.textContent = String(getTotalData(arr, "fat"));
        totalfoodC.textContent = String(getTotalData(arr, "carbo"));
        totalfoodCal.textContent = String(getTotalData(arr, "cal"));
    };

    const calcFoodPFC = (id: string | null, foodName: string, foodWeight: number) => {
        const search = new FoodDataSearch(foodName);
        const foodP = document.querySelector(`#foodP${id} dd span`) as HTMLSpanElement;
        const foodF = document.querySelector(`#foodF${id} dd span`) as HTMLSpanElement;
        const foodC = document.querySelector(`#foodC${id} dd span`) as HTMLSpanElement;
        const foodCal = document.querySelector(`#foodCal${id} dd span`) as HTMLSpanElement;
        const rate = foodWeight / search.data.baseWeight;
        const protein = finalize(search.data.protein, rate);
        const fat = finalize(search.data.fat, rate);
        const carbo = finalize(search.data.carbo, rate);
        const cal = finalize(search.data.baseCal, rate);
        foodP.textContent = String(protein);
        foodF.textContent = String(fat);
        foodC.textContent = String(carbo);
        foodCal.textContent = String(cal);
        return {
            protein,
            fat,
            carbo,
            cal,
        };
    };

    const foodDataOutput = (ev: React.ChangeEvent) => {
        const id = ev.target.getAttribute("data-id");
        const select = document.getElementById(`select${id}`) as HTMLSelectElement;
        const input = document.getElementById(`input${id}`) as HTMLInputElement;
        const referenceWeight = Number(input.value);
        calcFoodPFC(id, select.value, referenceWeight);
        const foodPFC = calcFoodPFC(id, select.value, referenceWeight);
        const updateList = stateFoods.concat([]);
        let hitIndex = 0;
        for (const i in updateList) {
            if (updateList[i].id === Number(id)) {
                hitIndex = Number(i);
                updateList[hitIndex] = {
                    protein: foodPFC.protein,
                    fat: foodPFC.fat,
                    carbo: foodPFC.carbo,
                    cal: foodPFC.cal,
                    id: Number(id),
                    name: select.value,
                    referenceWeight: Number(input.value),
                };
            }
        }
        calcFoodsTotalPFC(updateList);
        setStateFoods(updateList);
        seve(updateList);
    };

    const addFood = () => {
        const updateList = stateFoods.concat([]);
        updateList.push({
            id: getId(),
            name: "食品を選択して下さい",
            referenceWeight: 100,
            protein: 0,
            fat: 0,
            carbo: 0,
            cal: 0,
        });
        setStateFoods(updateList);
    };

    const showModal = (ev: React.MouseEvent) => {
        const btn = ev.target as HTMLButtonElement;
        const id = btn.getAttribute("data-id");
        const modal = document.getElementById("modal") as HTMLDivElement;
        const modalUl = document.getElementById("modalUl") as HTMLUListElement;
        modal.classList.add("isShown");
        modalUl.setAttribute("data-id", String(id));
    };

    const hiddenModal = () => {
        const modal = document.getElementById("modal") as HTMLDivElement;
        modal.classList.remove("isShown");
    };

    const chooseFood = (ev: React.MouseEvent) => {
        const btn = ev.target as HTMLButtonElement;
        const ul = btn.closest("ul") as HTMLUListElement;
        const modal = document.getElementById("modal") as HTMLDivElement;
        const btnValue = btn.value;
        const id = ul.getAttribute("data-id");
        const showModalBtn = document.getElementById(`showModal${id}`) as HTMLButtonElement;
        const select = document.getElementById(`select${id}`) as HTMLSelectElement;
        const m = select.length;
        for (let i = 0; m > i; i += 1) {
            if (select.options[i].value === btnValue) {
                select.options[i].selected = true;
                select.dispatchEvent(new Event("change", { bubbles: true, composed: true }));
            }
        }
        modal.classList.remove("isShown");
        showModalBtn.textContent = btnValue;
    };

    const showFoodData = (ev: React.MouseEvent) => {
        const btn = ev.currentTarget as HTMLButtonElement;
        const id = btn.getAttribute("data-id");
        const target = document.getElementById(`foodData${id}`) as HTMLDivElement;
        target.classList.toggle("show");
    };

    const chooseCategory = (ev: React.ChangeEvent) => {
        const select = ev.target as HTMLSelectElement;
        const value = select.value;
        if (value === "0") {
            setStateCategory("");
        } else {
            setStateCategory(value);
        }
    };

    // bindしたい
    const deleteFood = () => {
        return (ev: React.MouseEvent) => {
            const btn = ev.currentTarget as HTMLButtonElement;
            const currentId = Number(btn.getAttribute("data-id"));
            const updateList = stateFoods.concat([]);
            const idList = [];
            for (const i in updateList) {
                if (updateList[i].id === currentId) {
                    delete updateList[i];
                } else {
                    idList.push(updateList[i].id);
                }
            }
            const cloneArr = updateList.filter(v => v);
            setStateFoods(cloneArr);
            calcFoodsTotalPFC(cloneArr);
            seve(cloneArr);
        };
    };

    const showconsole = () => {
        console.log(stateFoods);
        console.log("Page", statePage);
        console.log("pageIndex", statePageIndex);
    };

    const seve = (arr: FoodsData[]) => {
        localStorage.setItem("foods", JSON.stringify(arr));
    };

    const saveWeight = (weight: string) => {
        localStorage.setItem("bodyWeight", weight);
    };

    const changePage = (ev: React.MouseEvent) => {
        const btn = ev.currentTarget as HTMLButtonElement;
        const value = btn.value;
        const pageLength = statePage.length;
        if (value === "prev" && statePageIndex !== 0) {
            setStatePageIndex(prevCount => prevCount - 1);
        } else if (value === "next" && statePageIndex < pageLength - 1) {
            setStatePageIndex(prevCount => prevCount + 1);
        }
    };

    const newList = [];
    const newOptionList = [];
    const categories = [];
    const categoryList = [];
    for (const i in initialFoods) {
        for (const j in initialFoods[i].name) {
            if (Number(i) !== 0) {
                if (!stateCategory || initialFoods[i].category.indexOf(stateCategory) !== -1) {
                    newList.push(
                        <li key={`${i}${j}`}>
                            <button value={initialFoods[i].name[j]} onClick={chooseFood}>
                                {initialFoods[i].name[j]}
                            </button>
                        </li>
                    );
                }
            }
            newOptionList.push(<option key={`${i}${j}`}>{initialFoods[i].name[j]}</option>);
        }

        // categoryを全部categoriesに格納
        for (const k in initialFoods[i].category) {
            if (Number(i) !== 0) {
                categories.push(initialFoods[i].category[k]);
            }
        }
    }
    // 重複している名前を削除
    const deduplication = categories.filter((x, i, self) => {
        return self.indexOf(x) === i;
    });
    for (const m in deduplication) {
        categoryList.push(
            <option key={m} value={deduplication[m]}>
                {deduplication[m]}
            </option>
        );
    }

    const list = [];
    for (const i in stateFoods) {
        const id = String(stateFoods[i].id);
        const name = stateFoods[i].name;
        const referenceWeight = String(stateFoods[i].referenceWeight);
        let deleteBtnClass;
        if (stateFoods.length > 1) {
            deleteBtnClass = "active";
        }
        list.push(
            <div id={`id${id}`} data-id={id} key={i} className="foodList">
                <ul>
                    <li className="selectWrap">
                        <button
                            data-id={id}
                            onClick={showModal}
                            id={`showModal${id}`}
                            className="showModal"
                        >
                            {name}
                        </button>
                        <select
                            id={`select${id}`}
                            data-id={id}
                            onChange={foodDataOutput}
                            value={name}
                        >
                            {newOptionList}
                        </select>
                    </li>
                    <li className="inputWrap">
                        <input
                            id={`input${id}`}
                            data-id={id}
                            type="number"
                            value={referenceWeight}
                            onChange={foodDataOutput}
                        />
                        g
                    </li>
                    <li className="detailBtnWrap">
                        <button data-id={id} onClick={showFoodData}>
                            <i className="material-icons">list</i>
                        </button>
                    </li>
                    <li className="deleteBtnWrap">
                        <button data-id={id} onClick={deleteFood()} className={deleteBtnClass}>
                            <i className="material-icons">delete</i>
                        </button>
                    </li>
                </ul>
                <div id={`foodData${id}`} className="foodDataWrap">
                    <dl className="foodP" id={`foodP${id}`}>
                        <dt>タンパク質</dt>
                        <dd>
                            <span>0</span>g
                        </dd>
                    </dl>
                    <dl className="foodF" id={`foodF${id}`}>
                        <dt>脂質</dt>
                        <dd>
                            <span>0</span>g
                        </dd>
                    </dl>
                    <dl className="foodC" id={`foodC${id}`}>
                        <dt>炭水化物</dt>
                        <dd>
                            <span>0</span>g
                        </dd>
                    </dl>
                    <dl className="foodCal" id={`foodCal${id}`}>
                        <dt>カロリー</dt>
                        <dd>
                            <span>0</span>kcal
                        </dd>
                    </dl>
                </div>
            </div>
        );
    }

    const setPage = [];
    let prevBtn;
    let nextBtn;
    const statePageLength = statePage.length;
    for (const i in statePage) {
        if (Number(i) !== 0) {
            prevBtn = (
                <button onClick={changePage} value="prev">
                    <i className="material-icons">keyboard_arrow_left</i>
                </button>
            );
        }
        console.log("i", i, "statePageLength", statePageLength);
        if (Number(i) !== statePageLength - 1) {
            nextBtn = (
                <button onClick={changePage} value="next">
                    <i className="material-icons">keyboard_arrow_right</i>
                </button>
            );
        }
        if (statePageIndex === Number(i)) {
            setPage.push(
                <div key={i} className="foodsWrap" data-id={i}>
                    <div className="titleWrap">
                        {prevBtn}
                        <input type="text" value={statePage[i].title} />
                        {nextBtn}
                    </div>
                    {list}
                </div>
            );
        }
    }

    return (
        <div className="foodAdjuster">
            <main id="main">
                <div className="separater">
                    <div className="bodyWeight wrap">
                        体重
                        <div className="input">
                            <input
                                type="number"
                                onChange={inputBodyWeight}
                                value={stateBodyWeight}
                            />
                            kg
                        </div>
                        <button name="add" onClick={addFood}>
                            <i className="material-icons">add</i>
                        </button>
                    </div>
                    <div className="dataWrap">
                        <div className="humanData">
                            <p className="title">必要な栄養</p>
                            <dl id="totalP">
                                <dt>タンパク質</dt>
                                <dd>
                                    <span>0</span>g
                                </dd>
                            </dl>
                            <dl id="totalF">
                                <dt>脂質</dt>
                                <dd>
                                    <span>0</span>g
                                </dd>
                            </dl>
                            <dl id="totalC">
                                <dt>炭水化物</dt>
                                <dd>
                                    <span>0</span>g
                                </dd>
                            </dl>
                            <dl id="totalCal">
                                <dt>総摂取カロリー</dt>
                                <dd>
                                    <span>0</span>kcal
                                </dd>
                            </dl>
                        </div>
                        <div className="foodsData">
                            <p className="title">食事の栄養</p>
                            <dl id="totalFoodP">
                                <dt>タンパク質</dt>
                                <dd>
                                    <span>0</span>g
                                </dd>
                            </dl>
                            <dl id="totalFoodF">
                                <dt>脂質</dt>
                                <dd>
                                    <span>0</span>g
                                </dd>
                            </dl>
                            <dl id="totalFoodC">
                                <dt>炭水化物</dt>
                                <dd>
                                    <span>0</span>g
                                </dd>
                            </dl>
                            <dl id="totalFoodCal">
                                <dt>総摂取カロリー</dt>
                                <dd>
                                    <span>0</span>kcal
                                </dd>
                            </dl>
                        </div>
                    </div>
                    {setPage}
                </div>
                <aside className="modal" id="modal">
                    <button className="close" onClick={hiddenModal}>
                        <i className="material-icons">close</i>
                    </button>
                    <select onChange={chooseCategory}>
                        <option value="0">カテゴリを選択</option>
                        {categoryList}
                    </select>
                    <ul data-id="0" id="modalUl">
                        {newList}
                    </ul>
                </aside>
                <button onClick={showconsole}>console</button>
            </main>
        </div>
    );
};

export default test;
