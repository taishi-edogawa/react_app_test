import * as React from 'react';
import './App.css';

import Header from 'components/Header';
import Main from 'components/Main';
import SideMenu from 'components/SideMenu';

class App extends React.Component {
    public render() {
        return (
            <div className="App">
                <Header />
                <div className="wrap">
                    <SideMenu />
                    <Main />
                </div>
            </div>
        );
    }
}

export default App;
