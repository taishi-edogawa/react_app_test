import * as React from 'react';

export interface ITopMenu {
    category: string;
}

const topMenu = React.createContext<ITopMenu | null>(null);

export const topMenuProvider = topMenu.Provider;

export const topMenuConsumer = topMenu.Consumer;
