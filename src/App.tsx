import * as React from "react";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/icons/Menu";
import { Theme } from "@material-ui/core/styles/createMuiTheme";
import { createStyles, withStyles, WithStyles } from "@material-ui/core/styles";
import { Route, Switch } from "react-router";
import test from "components/Main/test";
import test2 from "components/Main/test2";
import "./App.css";

import Header from "components/Header";
import Main from "components/Main";
import SideMenu from "components/SideMenu";
import Tab from "components/Main/Tab";

const drawerWidth = 240;

const styles = (theme: Theme) =>
    createStyles({
        root: {
            backgroundColor: "#c8c5c8",
            display: "flex",
            minHeight: 450,
            padding: "10px 10px 40px",
        },
        drawer: {
            [theme.breakpoints.up("sm")]: {
                minHeight: "50vh",
                width: drawerWidth,
                flexShrink: 0,
                position: "relative",
            },
        },
        appBar: {
            marginLeft: drawerWidth,
            [theme.breakpoints.up("sm")]: {
                width: `calc(100% - ${drawerWidth}px)`,
            },
        },
        menuButton: {
            marginRight: 20,
            [theme.breakpoints.up("sm")]: {
                display: "none",
            },
        },
        toolbar: theme.mixins.toolbar,
        drawerPaper: {
            backgroundColor: "#c8c5c8",
            border: "none",
            position: "absolute",
            width: drawerWidth,
        },
        content: {
            flexGrow: 1,
            marginLeft: theme.spacing.unit * 3,
            padding: theme.spacing.unit * 3,
        },
    });

export interface Props extends WithStyles<typeof styles> {
    theme: Theme;
}

interface MobileOpen {
    mobileOpen: boolean;
}

// tslint:disable-next-line: variable-name
const App = withStyles(styles)(
    class extends React.Component<Props, MobileOpen> {
        state = {
            mobileOpen: false,
        };

        handleDrawerToggle = () => {
            this.setState(state => ({ mobileOpen: !state.mobileOpen }));
        };
        public render() {
            const { classes, theme } = this.props;
            return (
                <div className="App">
                    <Switch>
                        <Route path="/test" component={test} />
                        <Route path="/test2" component={test2} />
                    </Switch>
                </div>
            );
        }
    }
);

export default withStyles(styles, { withTheme: true })(App);
